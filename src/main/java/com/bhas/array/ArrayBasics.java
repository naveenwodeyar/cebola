package com.bhas.array;

import java.util.ArrayList;
import java.util.Arrays;

public class ArrayBasics 
{
	// 1. Sum of the elements in the array,
	static double sumOfArray(int[] num)
	{
		int sum=0;
		for(int i=0; i<=num.length-1; i++)
			sum +=num[i];
		return sum;
	}
	
	// 2. String to Array,
	static void stringToArray(String st)
	{
		char[] ch = st.toCharArray();
		System.out.println("\nString given,"+st);
		System.out.println("\n Array obtained,"+Arrays.toString(ch));
	}
	
	// 3. Array to ArrayList,
	static ArrayList<String> arrToArrList(String[] names)
	{
		return new ArrayList<>(Arrays.asList(names));
	}
	
	// 4. Second largest element in the array, using swapping
	static void secLargetstElementInArray(int[] num)
	{
		System.out.println("\n Array given,"+Arrays.toString(num));
		
		int largest = 0;
		int secLargest = 0;
		try
		{
			for(int i=0; i<=num.length; i++)
			{
				if(num[i] > largest)
				{
					secLargest = largest;
					largest = num[i];
				}
				else if(num[i] > secLargest)
				{
					secLargest = num[i];
				}
			}
		}
		catch(ArrayIndexOutOfBoundsException e)
		{
			e.getLocalizedMessage();
		}
		finally 
		{
			System.out.println("Largest element in the array="+largest);
			System.out.println("Second Largest element in the array="+secLargest);
		}
		
	}
	
	public static void main(String[] args) 
	{
		int[] num = {9,5,8,4,7,6,3,2};
		System.out.println("\n Basic Array operations,\n");
		System.out.println("\n Sum of the elements in the array,"+sumOfArray(new int[]{1,5,6,8,9}));
		System.out.println("\n***********");
		stringToArray("Passport Seva");
		System.out.println("\n***********");
		ArrayList<String> list = arrToArrList(new String[] {"A","B","C","D"});
		System.out.println(list);
		System.out.println("\n***********");
		secLargetstElementInArray(num);
		System.out.println("\n***********");
	}

}
