package com.bhas.array;

import java.util.*;

public class ArraySorting 
{
	// 1. second largest element in the array,
	static void secLargetstElementInArray(int[] num)
	{
		System.out.println(Arrays.toString(num));
		List<Integer> list = new ArrayList(Arrays.asList(num));
					Integer min = list.stream().sorted(Comparator.reverseOrder()).skip(1).findFirst().get();
					System.out.println(min);
				
	}
	
	public static void main(String[] args) 
	{
		int[] num = {9,5,8,4,7,6,3,2};
		secLargetstElementInArray(num);
		System.out.println("***********");
	}

}
