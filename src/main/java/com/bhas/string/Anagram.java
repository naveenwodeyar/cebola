package com.bhas.string;

import java.util.Arrays;

public class Anagram 
{
	static boolean anagramTest(String s1,String s2)
	{
		char[] st1 = s1.toLowerCase().toCharArray();
		char[] st2 = s2.toLowerCase().toCharArray();
		
		if(st1.length != st2.length)
			return false;
		
		Arrays.sort(st2);
		Arrays.sort(st1);
		
		return Arrays.equals(st1, st2);		
	}
	
	public static void main(String[] args) 
	{
		System.out.println("\n An anagram of a string is another string that contains the same characters, only the order of characters can be different.");
		if(anagramTest("Silent", "Listen"))
			System.out.println("\nGiven strings are anagram string of each other");
		else
			System.out.println("\n Given strings are not anagram strings");
	
	}

}
