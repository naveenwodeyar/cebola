package com.bhas.string;

public class Palindrome
{
    static void palindromeTest(String st)
    {
        StringBuilder sb = new StringBuilder(st);

        if(st.contentEquals(sb))
            System.out.printf("Given string,"+st+" is an palindrome");
        else
            System.out.printf("Given string,"+st+" is not an palindrome");
    }

    public static void main(String[] args)
    {
        System.out.printf("\nPalindrome\n");
        palindromeTest("Level");
    }
}
