package com.bhas.string;

public class Palindrome2
{
    static void palindromeTest(String st)
    {
       String str = st.toLowerCase();
       String str1 = "";
       for(int i=str.length()-1; i>0; i--)
       {
    	   str1 = str1+str1.charAt(i);
       }
       System.out.println("Original string,"+str);
       System.out.println("Reversed string,"+str1);
       
       if(str1.equalsIgnoreCase(str))
    	   System.out.println("\n Given string is palindrome");
       else
    	   System.out.println("\n Given string is not an palindrome");
    }

    public static void main(String[] args)
    {
        System.out.printf("\nPalindrome program\n");
        palindromeTest("Level");
    }
}
