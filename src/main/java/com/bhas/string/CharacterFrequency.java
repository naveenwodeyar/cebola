package com.bhas.string;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;

public class CharacterFrequency
{
    static void charFreq(String st)
    {
        char[] str = st.toLowerCase().toCharArray();

        Map<Character,Integer> hp = new HashMap<>(5);

        for (char c: str)
        {
            if (hp.containsKey(c))
                hp.put(c, hp.get(c)+1);
            else
                hp.put(c,1);
        }
       hp.entrySet().stream().forEach(System.out::println);
    }

    public static void main(String[] args)
    {
        System.out.println("Character Frequency in an String\n");
        charFreq("Character");
    }
}
