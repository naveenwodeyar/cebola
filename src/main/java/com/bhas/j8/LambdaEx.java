package com.bhas.j8;

@FunctionalInterface
interface Test{
	String test();
}
public class LambdaEx {

	public static void main(String[] args) {
		Test t = ()->{System.out.println("Lambda Express");}
	}
}
